-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use work.lt16soc_peripherals.all;

ENTITY warmup1_tb IS
END ENTITY;

ARCHITECTURE sim OF warmup1_tb IS
	constant CLK_PERIOD : time := 10 ns;

	signal clk : std_logic := '0';
	signal rst : std_logic;

	signal led : std_logic_vector(7 downto 0);

BEGIN
	dut : lt16soc_top
		generic map(
			programfilename => "programs/blinky.ram"
		)
		port map(
			clk           => clk,
			rst           => rst,
			led           => led,
			sw            => open,
			btn           => open,
			dataLCD       => open,
			enableLCD     => open,
			rsLCD         => open,
			rwLCD         => open,
			ps2_keyb_clk  => open,
			ps2_keyb_data => open,
			driver_en     => open,
			rxd1          => open,
			txd1          => open
		);

	clk_gen : process
	begin
		clk <= not clk;
		wait for CLK_PERIOD / 2;
	end process clk_gen;

	stimuli : process
	begin
		rst <= '0';
		wait for CLK_PERIOD;
		rst <= '1';
		wait for 20000 * CLK_PERIOD;
		assert false report "Simulation terminated!" severity failure;
	end process stimuli;

END ARCHITECTURE;
