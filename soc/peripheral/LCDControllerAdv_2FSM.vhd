-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE work.lt16x32_global.all;
USE work.wishbone.all;
USE work.config.all;

ENTITY wb_lcd_adv_2FSM IS
	generic(
		memaddr  : generic_addr_type;
		addrmask : generic_mask_type
	);
	port(
		clk       : IN    std_logic;
		rst       : IN    std_logic;
		wslvi     : IN    wb_slv_in_type;
		wslvo     : OUT   wb_slv_out_type;
		dataLCD   : INOUT std_logic_vector(7 downto 0); -- bidirectional Data Bus
		enableLCD : OUT   std_logic;    -- Enable bus for read/write
		rsLCD     : OUT   std_logic;    -- Register select, HIGH for DR, LOW for IR
		rwLCD     : OUT   std_logic     -- Read/Write signal for Bus Direction, HIGH for Read, LOW for Write
	);
END ENTITY;

ARCHITECTURE behav OF wb_lcd_adv_2FSM IS
	-- FOR SIMULATION
	--		constant INIT_COUNTER_LIMIT    : integer := 1;
	--		constant WAIT_15_COUNTER_LIMIT : integer := 1;
	--		constant WAIT_37_COUNTER_LIMIT : integer := 1;
	--		constant WAIT_50_COUNTER_LIMIT : integer := 1;
	--		constant WAIT_70_COUNTER_LIMIT : integer := 1;


	constant INIT_COUNTER_LIMIT    : integer := 4000000;
	constant WAIT_15_COUNTER_LIMIT : integer := 1500000;
	constant WAIT_37_COUNTER_LIMIT : integer := 3700;
	constant WAIT_50_COUNTER_LIMIT : integer := 50;
	constant WAIT_70_COUNTER_LIMIT : integer := 70;

	signal lcd_reg : std_logic_vector(9 downto 0);
	signal ack     : std_logic;

	signal new_data, ctrl_busy : std_logic;

	type enable_states is (enable_reset, enable_wait, enable_high, enable_low);
	signal pre_enable_state, nx_enable_state : enable_states;
	signal cycle_e                           : boolean;
	signal E_edge                            : boolean;
	signal pre_count_e, nx_count_e           : integer range 0 to 70;

	type interface_states is (RESET, IFS, W1, FS1, W2, FS2, W3, DO, WND, W, B, W4, CLR, W5, ENT);
	signal pre_int_state, nx_int_state     : interface_states;
	signal pre_int_counter, nx_int_counter : integer;

BEGIN
	pre_int_state_proc : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				pre_int_state   <= RESET;
				pre_int_counter <= 0;
			else
				pre_int_state   <= nx_int_state;
				pre_int_counter <= nx_int_counter;
			end if;
		end if;
	end process pre_int_state_proc;

	nx_int_state_proc : process(pre_int_state, E_edge, lcd_reg(7 downto 0), lcd_reg(8), lcd_reg(9), new_data, pre_int_counter)
	begin
		-- default output behavior
		ctrl_busy      <= '1';
		dataLCD        <= "ZZZZZZZZ";
		rsLCD          <= '0';
		rwLCD          <= '0';
		cycle_e        <= false;
		nx_int_state   <= pre_int_state;
		nx_int_counter <= pre_int_counter;
		case pre_int_state is
			when RESET =>
				nx_int_counter <= 0;
				nx_int_state   <= IFS;

			when IFS =>
				nx_int_counter <= pre_int_counter + 1;
				if pre_int_counter = INIT_COUNTER_LIMIT then
					nx_int_counter <= 0;
					nx_int_state   <= W1;
				end if;

			-- FUNCTION SET --
			when W1 =>
				dataLCD <= X"38";
				cycle_e <= true;
				if E_edge = true then
					nx_int_state <= FS1;
				end if;

			when FS1 =>
				dataLCD        <= X"38";
				nx_int_counter <= pre_int_counter + 1;
				if pre_int_counter = WAIT_37_COUNTER_LIMIT then
					nx_int_counter <= 0;
					nx_int_state   <= W2;
				end if;

			when W2 =>
				dataLCD <= X"38";
				cycle_e <= true;
				if E_edge = true then
					nx_int_state <= FS2;
				end if;

			when FS2 =>
				dataLCD        <= X"38";
				nx_int_counter <= pre_int_counter + 1;
				if pre_int_counter = WAIT_37_COUNTER_LIMIT then
					nx_int_counter <= 0;
					nx_int_state   <= W3;
				end if;

			-- DISPLAY ON --
			when W3 =>
				dataLCD <= X"0F";
				cycle_e <= true;
				if E_edge = true then
					nx_int_state <= DO;
				end if;

			when DO =>
				dataLCD        <= X"0F";
				nx_int_counter <= pre_int_counter + 1;
				if pre_int_counter = WAIT_37_COUNTER_LIMIT then
					nx_int_counter <= 0;
					nx_int_state   <= W4;
				end if;

			-- CLEAR --
			when W4 =>
				dataLCD <= X"01";
				cycle_e <= true;
				if E_edge = true then
					nx_int_state <= CLR;
				end if;

			when CLR =>
				dataLCD        <= X"01";
				nx_int_counter <= pre_int_counter + 1;
				if pre_int_counter = WAIT_15_COUNTER_LIMIT then
					nx_int_counter <= 0;
					nx_int_state   <= W5;
				end if;

			-- SET ENTRY MODE --
			when W5 =>
				dataLCD <= X"06";
				cycle_e <= true;
				if E_edge = true then
					nx_int_state <= ENT;
				end if;

			when ENT =>
				dataLCD        <= X"06";
				nx_int_counter <= pre_int_counter + 1;
				if pre_int_counter = WAIT_37_COUNTER_LIMIT then
					nx_int_counter <= 0;
					nx_int_state   <= WND;
				end if;

			-- WAIT FOR NEW DATA --
			when WND =>
				ctrl_busy <= '0';
				if new_data = '1' then
					nx_int_state <= W;
				end if;
			when W =>
				cycle_e <= true;
				rsLCD   <= lcd_reg(9);
				rwLCD   <= lcd_reg(8);
				if lcd_reg(8) = '1' then
					dataLCD <= "ZZZZZZZZ";
				else
					dataLCD <= lcd_reg(7 downto 0);
				end if;
				if E_edge = true then
					nx_int_state <= B;
				end if;
			when B =>
				rsLCD <= lcd_reg(9);
				rwLCD <= lcd_reg(8);
				if lcd_reg(8) = '1' then
					dataLCD <= "ZZZZZZZZ";
				else
					dataLCD <= lcd_reg(7 downto 0);
				end if;
				nx_int_counter <= pre_int_counter + 1;
				if pre_int_counter = WAIT_15_COUNTER_LIMIT then
					nx_int_counter <= 0;
					nx_int_state   <= WND;
				end if;
		end case;
	end process nx_int_state_proc;

	pre_enable_state_proc : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				pre_enable_state <= enable_reset;
				pre_count_e      <= 0;
			else
				pre_enable_state <= nx_enable_state;
				pre_count_e      <= nx_count_e;
			end if;
		end if;
	end process pre_enable_state_proc;

	nx_enable_state_proc : process(pre_enable_state, cycle_e, pre_count_e)
	begin
		-- default behavior
		nx_enable_state <= pre_enable_state;
		nx_count_e      <= pre_count_e;
		E_edge          <= false;
		enableLCD       <= '0';
		case pre_enable_state is
			when enable_reset =>
				nx_count_e      <= 0;
				nx_enable_state <= enable_wait;
			when enable_wait =>
				if cycle_e = true then
					nx_enable_state <= enable_high;
				end if;
			when enable_high =>
				enableLCD  <= '1';
				nx_count_e <= pre_count_e + 1;
				if pre_count_e = WAIT_50_COUNTER_LIMIT then
					nx_enable_state <= enable_low;
					E_edge          <= true;
					nx_count_e      <= 0;
				end if;
			when enable_low =>
				nx_count_e <= pre_count_e + 1;
				if pre_count_e = WAIT_70_COUNTER_LIMIT then
					nx_enable_state <= enable_wait;
					nx_count_e      <= 0;
				end if;
		end case;
	end process nx_enable_state_proc;

	new_data <= '1' when (wslvi.stb = '1' and wslvi.cyc = '1' and wslvi.we = '1' and ctrl_busy = '0' and ack = '1') else '0';

	read_write_proc : process(clk)
	begin
		if clk'event and clk = '1' then
			if rst = '1' then
				ack                     <= '0';
				lcd_reg                 <= (others => '0');
				wslvo.dat(31 downto 16) <= (others => '0');
			else
				if wslvi.stb = '1' and wslvi.cyc = '1' then
					if wslvi.we = '1' and ctrl_busy = '0' then
						lcd_reg <= dec_wb_dat(wslvi.sel, wslvi.dat)(9 downto 0);
						ack     <= not ack;
					elsif wslvi.we = '0' then
						wslvo.dat(31 downto 16) <= "0000000" & ctrl_busy & dataLCD;
						ack                     <= not ack;
					else
						ack <= '0';
					end if;
				else
					ack <= '0';
				end if;
			end if;
		end if;
	end process read_write_proc;

	wslvo.dat(15 downto 0) <= (others => '0');
	wslvo.wbcfg            <= wb_membar(memaddr, addrmask);
	wslvo.ack              <= ack;

END ARCHITECTURE;
