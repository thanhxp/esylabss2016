-- See the file "LICENSE" for the full license governing this code. --

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lt16x32_global.all;
use work.wishbone.all;
use work.config.all;

entity wb_switch is
	generic(
		memaddr  : generic_addr_type;   --:= CFG_BADR_SWITCH;
		addrmask : generic_mask_type    --:= CFG_MADR_SWITCH;
	);
	port(
		clk   : in  std_logic;
		rst   : in  std_logic;
		btn   : in  std_logic_vector(6 downto 0);
		sw    : in  std_logic_vector(7 downto 0);
		wslvi : in  wb_slv_in_type;
		wslvo : out wb_slv_out_type
	);
end wb_switch;

architecture Behavioral of wb_switch is
	signal data : std_logic_vector(0 to 31);
	signal ack  : std_logic;

begin
	process(clk)
	begin
		if clk'event and clk = '1' then
			data(0 to 7)   <= sw;
			data(8 to 14)  <= btn;
			data(15 to 31) <= (others => '0');
			if rst = '1' then
				ack  <= '0';
				data <= x"00_00_00_00";
			else
				if wslvi.stb = '1' and wslvi.cyc = '1' then
					if wslvi.we = '0' then
						wslvo.dat <= data;
					end if;
					ack <= not ack;
				else
					ack <= '0';
				end if;
			end if;
		end if;
	end process;

	wslvo.ack   <= ack;
	wslvo.wbcfg <= wb_membar(memaddr, addrmask);

end Behavioral;