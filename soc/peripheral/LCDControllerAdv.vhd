LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE work.lt16x32_global.all;
USE work.wishbone.all;
USE work.config.all;

entity wb_lcd_adv is
	generic(
		memaddr  : generic_addr_type;
		addrmask : generic_mask_type
	);
	port(
		clk       : IN    std_logic;
		rst       : IN    std_logic;
		wslvi     : IN    wb_slv_in_type;
		wslvo     : OUT   wb_slv_out_type;
		dataLCD   : INOUT std_logic_vector(7 downto 0); -- bidirectional Data Bus
		enableLCD : OUT   std_logic;    -- Enable bus for read/write
		rsLCD     : OUT   std_logic;    -- Register select, HIGH for DR, LOW for IR
		rwLCD     : OUT   std_logic     -- Read/Write signal for Bus Direction, HIGH for Read, LOW for Write
	);
end entity wb_lcd_adv;

architecture RTL of wb_lcd_adv is
	-- FOR SIMULATION
	--	constant POWER_40MS_COUNTER_LIMIT       : integer := 1;
	--	constant W_TAS_WOEN_30NS_COUNTER_LIMIT  : integer := 1;
	--	constant W_TPW_WTEN_460NS_COUNTER_LIMIT : integer := 1;
	--	constant W_TAH_WOEN_10NS_COUNTER_LIMIT  : integer := 1;
	--	constant WAIT_37US_COUNTER_LIMIT        : integer := 1;
	--	constant WAIT_15MS_COUNTER_LIMIT        : integer := 1;

	constant POWER_40MS_COUNTER_LIMIT       : integer := 4000000;
	constant W_TAS_WOEN_30NS_COUNTER_LIMIT  : integer := 3;
	constant W_TPW_WTEN_460NS_COUNTER_LIMIT : integer := 50;
	constant W_TAH_WOEN_10NS_COUNTER_LIMIT  : integer := 70;
	constant WAIT_37US_COUNTER_LIMIT        : integer := 3700;
	constant WAIT_15MS_COUNTER_LIMIT        : integer := 1500000;

	signal lcd_reg : std_logic_vector(9 downto 0);
	signal ack     : std_logic;

	signal new_data  : std_logic;       -- Flag indicate Processor writing new command to LCD Ctrl
	signal ctrl_busy : std_logic;       -- Flag indicate LCD Ctrl processing current command

	type interface_states is (RESET, WPW,
		                      FSWoEn1, FSWtEn, FSWoEn2, WAIT37usFSDO,
		                      DOWoEn1, DOWtEn, DOWoEn2, WAIT37usDOCLR,
		                      CLRWoEn1, CLRWtEn, CLRWoEn2, WAIT15msCLRENT,
		                      ENTWoEn1, ENTWtEn, ENTWoEn2, WAIT37usENTCHAR,
		                      WND,
		                      CHARWoEn1, CHARWtEn, CHARWoEn2, WAIT37usCHAR
	);
	signal pre_int_state, nx_int_state : interface_states;
	signal pre_counter, nx_counter     : integer range 0 to 4000000;
begin
	pre_state_seq_proc : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				pre_int_state <= RESET;
				pre_counter   <= 0;
			else
				pre_int_state <= nx_int_state;
				pre_counter   <= nx_counter;
			end if;
		end if;

	end process pre_state_seq_proc;

	nx_state_comb_proc : process(pre_counter, pre_int_state, lcd_reg(7 downto 0), lcd_reg(8), lcd_reg(9), new_data)
	begin
		-- default output behavior
		ctrl_busy    <= '1';
		dataLCD      <= "ZZZZZZZZ";
		rsLCD        <= '0';
		rwLCD        <= '0';
		enableLCD    <= '0';
		nx_int_state <= pre_int_state;
		nx_counter   <= pre_counter;
		case pre_int_state is
			when RESET =>
				nx_counter   <= 0;
				nx_int_state <= WPW;
			when WPW =>
				nx_counter <= pre_counter + 1;
				if pre_counter = POWER_40MS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= FSWoEn1;
				end if;
			when FSWoEn1 =>
				dataLCD    <= X"38";
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TAS_WOEN_30NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= FSWtEn;
				end if;
			when FSWtEn =>
				dataLCD    <= X"38";
				enableLCD  <= '1';
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TPW_WTEN_460NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= FSWoEn2;
				end if;
			when FSWoEn2 =>
				dataLCD    <= X"38";
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TAH_WOEN_10NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= WAIT37usFSDO;
				end if;
			when WAIT37usFSDO =>
				nx_counter <= pre_counter + 1;
				if pre_counter = WAIT_37US_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= DOWoEn1;
				end if;
			---------------
			when DOWoEn1 =>
				dataLCD    <= X"0F";
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TAS_WOEN_30NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= DOWtEn;
				end if;
			when DOWtEn =>
				dataLCD    <= X"0F";
				enableLCD  <= '1';
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TPW_WTEN_460NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= DOWoEn2;
				end if;
			when DOWoEn2 =>
				dataLCD    <= X"0F";
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TAH_WOEN_10NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= WAIT37usDOCLR;
				end if;
			when WAIT37usDOCLR =>
				nx_counter <= pre_counter + 1;
				if pre_counter = WAIT_37US_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= CLRWoEn1;
				end if;
			when CLRWoEn1 =>
				dataLCD    <= X"01";
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TAS_WOEN_30NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= CLRWtEn;
				end if;
			when CLRWtEn =>
				dataLCD    <= X"01";
				enableLCD  <= '1';
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TPW_WTEN_460NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= CLRWoEn2;
				end if;
			when CLRWoEn2 =>
				dataLCD    <= X"01";
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TAH_WOEN_10NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= WAIT15msCLRENT;
				end if;
			when WAIT15msCLRENT =>
				nx_counter <= pre_counter + 1;
				if pre_counter = WAIT_15MS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= ENTWoEn1;
				end if;
			when ENTWoEn1 =>
				dataLCD    <= X"06";
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TAS_WOEN_30NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= ENTWtEn;
				end if;
			when ENTWtEn =>
				dataLCD    <= X"06";
				enableLCD  <= '1';
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TPW_WTEN_460NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= ENTWoEn2;
				end if;
			when ENTWoEn2 =>
				dataLCD    <= X"06";
				nx_counter <= pre_counter + 1;
				if pre_counter = W_TAH_WOEN_10NS_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= WAIT37usENTCHAR;
				end if;
			when WAIT37usENTCHAR =>
				nx_counter <= pre_counter + 1;
				if pre_counter = WAIT_37US_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= WND;
				end if;
			when WND =>
				ctrl_busy <= '0';
				if new_data = '1' then
					nx_int_state <= CHARWoEn1;
				end if;

			when CHARWoEn1 =>
				rsLCD <= lcd_reg(9);
				rwLCD <= lcd_reg(8);
				if lcd_reg(8) = '1' then
					dataLCD <= "ZZZZZZZZ";
				else
					dataLCD <= lcd_reg(7 downto 0);
				end if;
				nx_counter <= pre_counter + 1;
				if (pre_counter = W_TAS_WOEN_30NS_COUNTER_LIMIT) then
					nx_counter   <= 0;
					nx_int_state <= CHARWtEn;
				end if;
			when CHARWtEn =>
				rsLCD     <= lcd_reg(9);
				rwLCD     <= lcd_reg(8);
				enableLCD <= '1';
				if lcd_reg(8) = '1' then
					dataLCD <= "ZZZZZZZZ";
				else
					dataLCD <= lcd_reg(7 downto 0);
				end if;
				nx_counter <= pre_counter + 1;
				if (pre_counter = W_TPW_WTEN_460NS_COUNTER_LIMIT) then
					nx_counter   <= 0;
					nx_int_state <= CHARWoEn2;
				end if;
			when CHARWoEn2 =>
				rsLCD <= lcd_reg(9);
				rwLCD <= lcd_reg(8);
				if lcd_reg(8) = '1' then
					dataLCD <= "ZZZZZZZZ";
				else
					dataLCD <= lcd_reg(7 downto 0);
				end if;
				nx_counter <= pre_counter + 1;
				if (pre_counter = W_TAH_WOEN_10NS_COUNTER_LIMIT) then
					nx_counter   <= 0;
					nx_int_state <= WAIT37usCHAR;
				end if;
			when WAIT37usCHAR =>
				nx_counter <= pre_counter + 1;
				if pre_counter = WAIT_37US_COUNTER_LIMIT then
					nx_counter   <= 0;
					nx_int_state <= WND;
				end if;
		end case;
	end process nx_state_comb_proc;

	-- Logic for new_data signal
	new_data <= '1' when (wslvi.stb = '1' and wslvi.cyc = '1' and wslvi.we = '1' and ctrl_busy = '0' and ack = '1') else '0';

	read_write_proc : process(clk)
	begin
		if clk'event and clk = '1' then
			if rst = '1' then
				ack                     <= '0';
				lcd_reg                 <= (others => '0');
				wslvo.dat(31 downto 16) <= (others => '0');
			else
				if wslvi.stb = '1' and wslvi.cyc = '1' then
					if wslvi.we = '1' and ctrl_busy = '0' then
						lcd_reg <= dec_wb_dat(wslvi.sel, wslvi.dat)(9 downto 0);
						ack     <= not ack;
					elsif wslvi.we = '0' then
						wslvo.dat(31 downto 16) <= "0000000" & ctrl_busy & dataLCD;
						ack                     <= not ack;
					else
						ack <= '0';
					end if;
				else
					ack <= '0';
				end if;
			end if;
		end if;
	end process read_write_proc;

	wslvo.dat(15 downto 0) <= (others => '0');
	wslvo.wbcfg            <= wb_membar(memaddr, addrmask);
	wslvo.ack              <= ack;

end architecture RTL;
